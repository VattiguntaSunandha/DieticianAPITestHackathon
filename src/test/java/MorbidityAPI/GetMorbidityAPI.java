package MorbidityAPI;

import static io.restassured.RestAssured.given;

import org.junit.Assert;

import AppHooks.ApplicationHook;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetMorbidityAPI {
	{
		RestAssured.baseURI = "http://127.0.0.1:5000/api/";
		RestAssured.basePath = "Morbidity/";
		
	}
	RequestSpecification getrequest;
	Response Morbidityresponse;

	
	@Given("User sets Get request with endpoint")
	public void user_sets_get_request_with_endpoint() {
		getrequest = given().auth().preemptive().basic("KMSASM2022", "Dietician1!").contentType("application/json");
	}
	
	@When("User send Get request")
	public void user_send_get_request() {
		Morbidityresponse=getrequest.when().get();
	}
	
	@Then("Status {int} Ok")
	public void status_ok(Integer int1) {
		Morbidityresponse.then().log().all();
		int statuscode = Morbidityresponse.getStatusCode();
		Assert.assertEquals(statuscode, 200);
	}

}
